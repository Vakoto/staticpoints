﻿using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace StaticPoints
{
    [TestFixture]
    public class GetPermutationsTest
    {
        private int Factorial(int n)
        {
            return n == 0 ? 1 : Enumerable.Range(1, n).Aggregate((acc, x) => acc * x);
        }

        [Test]
        public void GetPermutations_ShouldWorkOnEmptySet()
        {
            Program.GetPermutations(0).Should().BeEmpty();
        }

        [Test]
        public void GetPermutations_ShouldReturnCorrectNumberOfPermutations()
        {
            for (var i = 1; i < 10; i++)
            {
                var expectedCount = Factorial(i);
                var actualCount = Program.GetPermutations(i).Count();

                actualCount.Should().Be(expectedCount);
            }
        }

        [Test]
        public void GetPermutations_ShouldReturnCorrectPermutations()
        {
            var expected = new[]
            {
                new[] {1, 2, 3},
                new[] {1, 3, 2},
                new[] {3, 1, 2},
                new[] {3, 2, 1},
                new[] {2, 3, 1},
                new[] {2, 1, 3}
            };

            Program.GetPermutations(3)
                .Should()
                .BeEquivalentTo(expected);
        }
    }
}