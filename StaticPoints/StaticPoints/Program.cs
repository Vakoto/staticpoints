﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace StaticPoints
{
    public class Program
    {
        private static void Swap(int[] source, int i, int j)
        {
            var t = source[i];
            source[i] = source[j];
            source[j] = t;
        }

        public static IEnumerable<IEnumerable<int>> GetPermutations(int n)
        {
            if (n == 0)
            {
                yield break;
            }

            var a = new int[n + 2];
            var p = new int[n + 2];
            var d = new int[n + 2];

            for (var i = 1; i <= n; i++)
            {
                a[i] = p[i] = i;
                d[i] = -1;
            }

            var m = 0;
            a[0] = a[n + 1] = n + 1;

            while (m != 1)
            {
                yield return a.Skip(1).Take(n);

                m = n;
                while (m > 1 && m < a[p[m] + d[p[m]]])
                {
                    d[p[m]] = -d[p[m]];
                    m--;
                }

                var position = p[m];
                var dx = d[position];

                Swap(p, m, a[position + dx]);
                Swap(a, position, position + dx);
                Swap(d, position, position + dx);
            }
        }

        private static int GetNumberOfStaticPoints(IEnumerable<int> permutation)
        {
            return permutation.Where((e, i) => e == i + 1).Count();
        }

        private static int GetNumberOfPermutationsWithKStaticPoints(int n, int k)
        {
            return GetPermutations(n).Count(permutation => GetNumberOfStaticPoints(permutation) == k);
        }

        static void Main(string[] args)
        {
            int n, k;
            using (var sr = new StreamReader("INPUT.TXT"))
            {
                var splitLine = sr.ReadLine().Split(' ');
                n = int.Parse(splitLine[0]);
                k = int.Parse(splitLine[1]);
            }

            using (var sw = new StreamWriter("OUTPUT.TXT"))
            {
                sw.WriteLine(GetNumberOfPermutationsWithKStaticPoints(n, k));
            }
        }
    }
}
